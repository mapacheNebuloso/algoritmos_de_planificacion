from cronometro import Cronometro
from proceso import Proceso
import time


class HRN:
	
	def __init__(self, procesos, linea_tiempo,lim_atenciones, quantum):
		self.linea_tiempo = linea_tiempo
		self.procesos = procesos
		self.procesos_en_cola = list()
		self.cronometro = Cronometro()
		
  
	def ejecutar(self):
     	
		self.cronometro.start()
		faltan_procesos = True
  
		while(faltan_procesos):
			self.agregar_proceso_cola(int(self.cronometro.now()))			#agrega los procesos que faltan por finalizar
			self.priorizar_cola()	

    
			if len(self.procesos_en_cola)>0:
				print(self.procesos_en_cola)

				proceso_actual = self.procesos_en_cola[0]						#Se toma el primer proceso en cola
			   
							
				while not proceso_actual.finish:
			
					print ("Atendiendo al proceso: ",proceso_actual.pid)
					time.sleep(1)												# simulamos la atencion de 1 segundo 
					proceso_actual.restar_t(1.0)								# a la rafaga se le resta el tiempo atendido
					print("el tiempo de ultima at de este proceso deberia ser: ",1.0 +proceso_actual.t_ultima_atencion, "y es:", )
					

					print("y es: ", proceso_actual.t_ultima_atencion)
     
					#"Sacar" el proceso de la cola
					if proceso_actual.rafaga == 0.0 :							#si el proceso ya termino su t_ejecucion
						proceso_actual.t_salida = int(self.cronometro.now())			#set t_salida
						proceso_actual.finish = True							#se finaliza el proceso 
						print ("Proceso: ",proceso_actual.pid,"finalizo")
						print (proceso_actual)

			#Este for solo mira si algun proceso falta por terminar
			faltan_procesos = False
			for p in self.procesos :
				if not p.finish :
					faltan_procesos = True
  
  
	def priorizar_cola(self):
		#se calcula la prioridad de cada uno de los procesos
		for p in self.procesos_en_cola:
			print(p.pid)	
			if not p.finish :											# puede que a un proceso que ya finalizo se le asigne de nuevo una prioridad que sea mas alta que uno que no 
				print ("---NOW: ",int(self.cronometro.now()))
				print ("---Tultima atencion",p.t_ultima_atencion)
				print("----TEsperando",p.t_espera)  

				p.t_espera = (int)(self.cronometro.now()-p.t_llegada) #se acumula el tiempo que el proceso no ha sido atendido
				p.prioridad = self.calc_prioridad(p.t_espera,p.t_ejecucion)	#se calcula la prioridad *interesante*
			else:														# si el proceso finalizo se mata la 
				p.prioridad = 0											#atencion ponindole prioridad de 0
		self.procesos_en_cola.sort(key=lambda x: x.prioridad, reverse = True)
			
     
  
	def calc_prioridad(self, t_espera, t_ejecucion):
		
		return ((t_espera+t_ejecucion)/t_ejecucion)
		

	''''
 		Este metodo permite meter en cola los procesos cuyo tiempo de llegada es alcanzado por el tiempo
		actual y asi, la cola se va llenando con nuevos procesos.
	'''
	def agregar_proceso_cola(self, t_actual):						#llega el tiempo actual
     
		for proceso in self.procesos:								#recorre los procesos
			if not proceso.finish:									#descartamos los procesos que ya finalizaron
				if proceso.t_llegada <= t_actual:					#si el tiempo actual es mayor al t_llegada 
					en_lista = False								#damos por hecho que el elemento no esta en cola
					for proceso_en_cola in self.procesos_en_cola:	#recorre los procesos en cola
						if proceso.pid == proceso_en_cola.pid:		#buscamos si el proceso ya se encuentra en cola
							en_lista = True							#notifica que el proceso si esta en la cola
					
					if not en_lista:								# sino esta en la cola
						self.procesos_en_cola.append(proceso)		# lo agrega
						print("Entra el proceso", proceso.pid)		
  
		
		