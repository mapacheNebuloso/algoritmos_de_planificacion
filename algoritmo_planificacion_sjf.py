from cronometro import Cronometro
import time
class SJF:
	
	def __init__(self, procesos, linea_tiempo, lim, quantum):
		print("Selecciono SJF")	
		self.procesos = procesos
		self.procesos_en_cola = list()
		self.terminado = False
		self.linea_tiempo = linea_tiempo

	def ejecutar(self):
		cronometro = Cronometro()
		cronometro.start()
		contador_tiempo = 0
		while not self.terminado:
			proceso_actual = None
			t_actual = int(cronometro.now())
			self.agregar_proceso_cola(t_actual) #cada vez que se finaliza un proceso, se invoca el metodo agregar_proceso_cola

			while contador_tiempo < t_actual:
				self.linea_tiempo.append([0 for i in range(len(self.procesos))])
				contador_tiempo += 1
			
			selecciono_primero = False
			if proceso_actual == None:
				for proceso in self.procesos_en_cola:	#se recorre la lista de procesos en cola
					if not proceso.finish and not selecciono_primero:	#con el fin de encontrar el primero que  no haya finalizado
						proceso_actual = proceso
						selecciono_primero = True

			for i in range(1, len(self.procesos_en_cola)):
				if not self.procesos_en_cola[i].finish:
					if proceso_actual.t_ejecucion > self.procesos_en_cola[i].t_ejecucion:	#buscar el proceso con menor tiempo de ejecucion de todos los que estan en cola
						proceso_actual = self.procesos_en_cola[i]

			# Ejecucion del proceso
			if proceso_actual != None:
				time.sleep(proceso_actual.t_ejecucion)

				t_actual = int(cronometro.now())
				while contador_tiempo < t_actual:
					self.linea_tiempo.append([0 for i in range(len(self.procesos))])
					if contador_tiempo >= proceso_actual.t_llegada:
						self.linea_tiempo[contador_tiempo][proceso_actual.pid] = 1
					contador_tiempo += 1

				print("termino el proceso:", proceso_actual.pid, cronometro.now())
				proceso_actual.finish = True		

				faltan_procesos = False						#damos por hecho que faltan procesos 
				for proceso in self.procesos:				#recorremos la cola 
					if not proceso.finish:					#si hay alguno que no se ha finalizado 
						faltan_procesos = True				#entonces se notifica que faltan procesos por ejecutarse

				if not faltan_procesos:						#si no faltan procesos 
					self.terminado = True					#se da por terminado el algoritmo

		print("El programa finalizo")

	def mostrar_estado(self):
		for proceso in self.procesos_en_cola :
			print(proceso)

	def agregar_proceso_cola(self, t_actual):						#llega el tiempo actual
     
		for proceso in self.procesos:								#recorre los procesos
			if not proceso.finish:									#si el procesos no ha finalizado
				if proceso.t_llegada <= t_actual:					#si el tiempo actual es mayor al t_llegada 
					en_lista = False								#damos por hecho que el elemento no esta en cola
					for proceso_en_cola in self.procesos_en_cola:	#recorre los procesos en cola
						if proceso.pid == proceso_en_cola.pid:		#si el pid se encuentra en la cola
							en_lista = True							#notifica que el proceso si esta en la lista
					
					if not en_lista:								# sino esta en la lista
						self.procesos_en_cola.append(proceso)		# lo agrega
						print("Entra el proceso", proceso.pid)