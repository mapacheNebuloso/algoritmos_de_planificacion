from cronometro import Cronometro
import time
class RR:
	
	def __init__(self, procesos, linea_tiempo,lim_atenciones, quantum):
		print("Selecciono RR")	
		self.procesos = procesos
		self.procesos_en_cola = list()
		self.terminado = False
		self.linea_tiempo = linea_tiempo
		self.lim_atenciones = lim_atenciones
		self.quantum = quantum

	def ejecutar(self):
		cronometro = Cronometro()
		cronometro.start()
		contador_tiempo = 0
		self.procesos.sort(key=lambda x: x.t_llegada) 
		while not self.terminado:
			proceso_actual = None
			t_actual = int(cronometro.now())
			self.agregar_proceso_cola(t_actual) #cada vez que se finaliza un proceso, se invoca el metodo agregar_proceso_cola
			while contador_tiempo < t_actual:
				self.linea_tiempo.append([0 for x in range(len(self.procesos))])
				contador_tiempo += 1
			selecciono_primero = False
			if proceso_actual == None:
				#Este while recorre la lista de procesos en cola. Buscando un primer proceso sin terminar y sacando de la cola a los que ya terminaron (MAPACHE)
				i = 0
				while i < len(self.procesos_en_cola):	#Hasta una posicion antes para que no se quede en bucle
					print("i:",i)	
					if not self.procesos_en_cola[i].finish:								#no haya finalizado
						if not selecciono_primero:
							proceso_actual = self.procesos_en_cola[i]						#lo selecciona como proceso actual
							selecciono_primero = True										#y cambia a true (de que ya selecciono uno)
							print("Tomo el proceso ",proceso_actual.pid)
						i += 1															
					elif self.procesos_en_cola[i].finish:								#si el proceso ya termino
						self.procesos_en_cola.pop(i)									#lo saca de la cola, lo que a su vez, le resta una posicion a los elementos a la derecha del elemento popeado

			# Ejecucion del proceso (simulacion)
			if proceso_actual != None:
				if proceso_actual.rafaga < self.quantum:
					time.sleep(proceso_actual.rafaga)
				else:
					time.sleep(self.quantum)

				t_actual = int(cronometro.now())
				print("p_actual", proceso_actual.pid, "rafaga", proceso_actual.rafaga)
				while contador_tiempo < t_actual:
					self.linea_tiempo.append([0 for x in range(len(self.procesos))])
					if contador_tiempo >= proceso_actual.t_llegada:
						self.linea_tiempo[contador_tiempo][proceso_actual.pid] = 1
					contador_tiempo += 1
				self.agregar_proceso_cola(t_actual)							#se agg los procesos que pudieron haber llegado durante el quantum
				self.procesos_en_cola.pop(0)								#se le quita la atencion al proceso actual 
				self.procesos_en_cola.append(proceso_actual)				#sacandolo y mandandolo al final de la cola
    
				if proceso_actual.rafaga < self.quantum:			#si la rafaga del proceso era menor al quantum	
					proceso_actual.rafaga = 0.0								#se deja en 0.0 
				else:
					proceso_actual.rafaga -= self.quantum			#sino, se le resta el quantum
				print("se resta quantum a rafaga ")
				#lim_atenciones, es la cantidad de atenciones de x segundos(quantum) que se le van a dar a cada uno de los procesos
				proceso_actual.num_atenciones += 1
				if self.lim_atenciones != 0:
					if proceso_actual.num_atenciones >= self.lim_atenciones:
						proceso_actual.finish = True

				if proceso_actual.rafaga <= 0:
					proceso_actual.finish = True
					print("termino el proceso:", proceso_actual.pid, cronometro.now())	
     
			#Este for debe estar por fuera de si hay o no proceso_actual. Se necesita para determinar si los procesos
			#han finalizado sin importar si hay o no proceso seleccionado.
			#igualmente cumple la funcion si hay proceso_actual
			faltan_procesos = False						#damos por hecho que faltan procesos 
			for proceso in self.procesos:				#recorremos la cola 
				if not proceso.finish:					#si hay alguno que no se ha finalizado 
					faltan_procesos = True				#entonces se notifica que faltan procesos por ejecutarse

			if not faltan_procesos:						#si no faltan procesos 
				self.terminado = True					#se da por terminado el algoritmo

		print("El programa finalizo")



	def agregar_proceso_cola(self, t_actual):						#llega el tiempo actual
     
		for proceso in self.procesos:								#recorre los procesos
			if not proceso.finish:									#si el procesos no ha finalizado
				if proceso.t_llegada <= t_actual:					#si el tiempo actual es mayor al t_llegada 
					en_lista = False								#damos por hecho que el elemento no esta en cola
					for proceso_en_cola in self.procesos_en_cola:	#recorre los procesos en cola
						if proceso.pid == proceso_en_cola.pid:		#si el pid se encuentra en la cola
							en_lista = True							#notifica que el proceso si esta en la lista
					
					if not en_lista:								# sino esta en la lista
						self.procesos_en_cola.append(proceso)		# lo agrega
						print("Entra el proceso", proceso.pid)

	