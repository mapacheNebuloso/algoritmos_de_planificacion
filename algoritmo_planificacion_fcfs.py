from cronometro import Cronometro
from proceso import Proceso
import time

class FCFS:
	
	def __init__(self, procesos, linea_tiempo, lim, quantum):
		self.procesos = procesos
		self.cronometro = Cronometro()
		self.terminado = False
		self.linea_tiempo = linea_tiempo

	def ejecutar(self):
		contador_tiempo = 0
		self.procesos.sort(key=lambda x: x.t_llegada)
		cronometro = Cronometro()
		cronometro.start()
		while contador_tiempo < self.procesos[0].t_llegada:
			self.linea_tiempo.append([0 for i in range(len(self.procesos))])
			contador_tiempo += 1
		time.sleep(contador_tiempo)
		for i in range(len(self.procesos)):
			print("atendiendo el proceso con id ", self.procesos[i])
			time.sleep(self.procesos[i].t_ejecucion)
			t_actual = int(cronometro.now())
			while contador_tiempo < t_actual:
					self.linea_tiempo.append([0 for i in range(len(self.procesos))])
					if contador_tiempo >= self.procesos[i].t_llegada:
						self.linea_tiempo[contador_tiempo][self.procesos[i].pid] = 1
					contador_tiempo += 1
			self.procesos[i].finish = True
		print("El programa finalizo")

	def ordenar_procesos(self):
		self.quick_sort(self.procesos, 0, len(self.procesos) - 1)

	def quick_sort(self, arreglo, ini, fin):
	    if len(arreglo) >= 2 and ini < fin:
	        if fin - ini == 1:
	            aux = arreglo[ini]
	            if arreglo[ini].t_llegada > arreglo[fin].t_llegada:
	                arreglo[ini] = arreglo[fin]
	                arreglo[fin] = aux
	        else:
	            menores = list()
	            mayores = list()
	            piv = fin
	            elementPiv = arreglo[piv]
	            for i in range(ini, fin):
	                if arreglo[i].t_llegada <= arreglo[piv].t_llegada:
	                    menores.append(arreglo[i])
	                elif arreglo[i].t_llegada > arreglo[piv].t_llegada:
	                    mayores.append(arreglo[i])

	            for i in range(0, len(menores)):
	                arreglo[ini + i] = menores[i]

	            piv = ini + len(menores)
	            arreglo[piv] = elementPiv

	            for i in range(0, len(mayores)):
	                arreglo[ini + len(menores) +  i + 1] = mayores[i]

	            arreglo = self.quick_sort(arreglo, ini, piv - 1)
	            arreglo = self.quick_sort(arreglo, piv + 1, fin)
	    return arreglo