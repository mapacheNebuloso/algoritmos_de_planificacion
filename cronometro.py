import time

class Cronometro:
    
    def __init__(self):
       self.inicio = 0.0
        
    def start(self):
        self.inicio = time.time()    
    
    def now(self):
        return time.time() - self.inicio
    
    
        