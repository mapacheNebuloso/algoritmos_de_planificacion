from cronometro import Cronometro
from algoritmo_planificacion_fcfs import FCFS
from algoritmo_planificacion_rr import RR
import time

class FMQ:
	
	def __init__(self, procesos, linea_tiempo, lim_atenciones, quantum):
		print("Selecciono FMQ")	
		self.procesos = procesos
		self.linea_tiempo = linea_tiempo
		

	def ejecutar(self):
		
		#-----------------------RR 1----------------------------------------------------------------------------
		rr1 = RR(self.procesos, self.linea_tiempo,1,2)
		rr1.ejecutar()
	
		#Todos los procesos que en ese ciclo no se terminaron, se setearon como finish. lo cual (esta mal)
  		#se cambia en este for para que el siguiente algoritmo los termine.
		todos_finalizados = True
		for p in self.procesos:
			if p.rafaga > 0.0 :
				p.finish = False
				todos_finalizados = False
  
		if todos_finalizados:
			print("Todos los procesos fueron finalizados en el RR 1")
		else:
			print("Paso al RR 2")
	
			#-----------------------RR 2-(lista_procesos, impresion, limite_De_atenciones, quantum)-----------------------
			rr2 = RR(self.procesos, self.linea_tiempo, 1, 4)
			rr2.ejecutar()

			todos_finalizados = True
			for p in self.procesos:
				if p.rafaga > 0.0 :
					todos_finalizados = False	
					p.finish = False	
					p.t_ejecucion = p.rafaga		#El FCFS termina de ejecutar lo que falte 
													#Se debe cambiar a la rafaga ya que si no se hace, el FCFS
													#va a ejecutar(sleep) por el t_ejecucion que traiga el proceso.
             

			if todos_finalizados:
				print("Todos los procesos fueron finalizados en el RR 2")
			else:
				print("Paso al FCFS")

				#-----------------------FCFS----------------------------------------------------------------------------

				fcfs = FCFS(self.procesos, self.linea_tiempo,0,0)
				fcfs.ejecutar()

				print("Termina el FMQ")
				return fcfs.linea_tiempo