class Proceso:

	t_ultima_atencion = 0.0
 
	def __init__(self, pid, t_ejecucion, quantum, t_llegada):
		self.pid = pid
		self.t_ejecucion = t_ejecucion
		self.quantum = quantum
		self.t_llegada = t_llegada
		self.t_salida = 0.0
		self.t_servicio = 0.0
		self.t_espera = 0.0 
		self.rafaga = t_ejecucion
		self.t_dedicado = 0.0 
		self.prioridad = 0.0
		self.num_atenciones = 0.0
		self.t_ultima_atencion = t_llegada			#de esta manera, si un proceso llega y NO es atendido  
  													#inmediatamente, se tendrá como t de referencia el tiempo
               										# de su llegada. Para calculos como t_espera
		self.finish = False
  
	def restar_t(self, resta):
		self.rafaga -= resta
	
	
  
		self.num_atenciones = 0
	def __repr__(self):
		return '(%s,%s,%s,%s)'%("PID: "+str(self.pid)," tEspera: "+str(self.t_espera)," tEjecucion: "+str(self.t_ejecucion)," Prioridad: "+str(self.prioridad))