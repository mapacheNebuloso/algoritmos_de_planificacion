from proceso import Proceso
from algoritmo_planificacion_fcfs import FCFS
from algoritmo_planificacion_sjf import SJF
from algoritmo_planificacion_srtf import SRTF
from algoritmo_planificacion_rr import RR
from algoritmo_planificacion_fmq import FMQ
from algoritmo_planificacion_hrn import HRN

import random


class Main:
    
    def __init__(self):

        self.procesos = list()
        self.crearProcesos(6)       # numero de procesos a ser creados para mandar al algoritmo en cuestion
        print("-----------------------------3------------------------------------------------------------------")
        print("Ingresa 1 para aplicar el algoritmo FCFS al conjunto de procesos")
        print("Ingresa 2 para aplicar el algoritmo SJF al conjunto de procesos")
        print("Ingresa 3 para aplicar el algoritmo SRTF al conjunto de procesos")
        print("Ingresa 4 para aplicar el algoritmo HRN al conjunto de procesos")
        print("Ingresa 5 para aplicar el algoritmo RR al conjunto de procesos")
        print("Ingresa 6 para aplicar el algoritmo FMQ al conjunto de procesos")
        
       
        
        num = input("Ingrese su eleccion: ")
        
        for proceso in self.procesos:
            print("id:", proceso.pid, "Tiempo ejecucion:", proceso.t_ejecucion, "Tiempo llegada:", proceso.t_llegada, "Quantum:", proceso.quantum)

        linea_tiempo = list()
        linea_tiempo.append([0 for i in range(len(self.procesos))])
        for i in range(len(self.procesos)):
            linea_tiempo[0][i] = self.procesos[i].pid

        linea_tiempo = self.switcher_algoritmos(num, linea_tiempo)
        self.imprimirMatriz(linea_tiempo)
        
        
    def switcher_algoritmos(self, numero, linea_tiempo):
        
        algoritmos = {
            "1" : FCFS,            
            "2" : SJF,
            "3" : SRTF, 
            "4" : HRN,
            "5" : RR, 
            "6" : FMQ
        }  

        al = algoritmos[numero](self.procesos, linea_tiempo, 0, 1)
        al.ejecutar()
        return al.linea_tiempo

    def crearProcesos(self, cant):
        for x in range(0,cant):
            t_ejec_rdm = random.randrange(1,6) # random de 1 a 6
            quantum_rdm = random.randrange(1,3) # random del 1 al 10 para el tiempo de atencion en el ciclo
            t_llegada_rdm = random.randrange(1,6) # random del 1 al 5
            pTemp = Proceso(x, t_ejec_rdm, quantum_rdm, t_llegada_rdm)    
            self.procesos.append(pTemp)
        # p1 = Proceso(2, 7, 1, 5)
        # self.procesos.append(p1)
        # p2 = Proceso(1, 5, 1, 5)
        # self.procesos.append(p2)
        # p3 = Proceso(0, 2, 1, 5)
        # self.procesos.append(p3)            

    def imprimirMatriz(self, matriz):
        msj = "\n        "

        num = len(str(len(self.procesos) - 1))
        for x in range(num):
            msj += " "
        for cont in range(1, len(matriz)):
            if len(str(cont)) > 1:
                msj += str(cont) + " "
            else:
                msj += str(cont) + "  "
        msj += "\n"


        for j in range(len(matriz[0])):
            for i in range(len(matriz)):
                if i == 0:
                    if len(str(len(self.procesos) - 1)) > 1:
                        if matriz[i][j] > 9:
                            msj += "PID " + str(matriz[i][j]) + " || "
                        else:
                            msj += "PID " + str(matriz[i][j]) + "  || "
                    else:
                        msj += "PID " + str(matriz[i][j]) + " || "
                else:
                    if matriz[i][j] == 0:
                        msj += ":  "
                    else:
                        msj += "X  "
            msj+= "\n"
        print(msj)

Main()            
            